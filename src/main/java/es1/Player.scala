import akka.actor.{Actor, ActorRef}
import akka.event.Logging

import scala.util.Random

class Player(var ticketDispenser: ActorRef) extends Actor {

     val log = Logging(context.system, this)

     var tryN: Int = new Random().nextInt(GUIActor.MAX_RAND)

     override def receive: Receive = {
          case Player.START =>
               ticketDispenser ! TicketDispenser.GET_TICKET
          case msg: Ticket =>
               context.parent ! Guess(msg.ticketN, tryN)
          case msg: Response =>
               ticketDispenser ! TicketDispenser.GET_TICKET
               tryN += msg.response
          case msg: NumberGuessed =>
               if (msg.winnerPath == context.self.path) log.info("I WON!!!")
               else {
                    log.info("BUHHUU")
                    context.stop(context.self)
               }
          case _  => log.info("received unknown message")
     }
}

object Player {
     val START: String = "start"
}
