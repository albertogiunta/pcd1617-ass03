import akka.actor.{Actor, ActorPath, ActorRef, ActorSystem, Props, Stash}
import akka.event.Logging
import es1.GameGUI

import scala.util.Random

class Oracle extends Actor with Stash {
     val log = Logging(context.system, this)

     var initVal: Start = _
     var playerList: List[ActorRef] = _

     override def receive: Receive = {
          case msg: Start =>
               initVal = msg
               context.parent ! new GuiUpdateInt(GUIActor.N_TO_GUESS, initVal.oracleN)
               context.parent ! new GuiUpdateInt(GUIActor.CURRENT_TURN, initVal.turn)
               spawnPlayers()
          case Oracle.STOP =>
               context.become(putInStop)
          case msg: Guess =>
               if (msg.ticketN != initVal.currentN) {
                    stash()
               } else {
                    if (!initVal.numberWasGuessed) {
                         initVal.currentN += 1
                         if (msg.guessNumber == initVal.oracleN) {
                              unstashAndKillAllPlayersAndTicketDispenser()
                              context.parent ! GuiUpdateString(GUIActor.WINNER, sender().path.name)
                              initVal.numberWasGuessed = true
                              context.become(putInStop)
                         }
                         else if (msg.guessNumber > initVal.oracleN) sender() ! Response(-1)
                         else if (msg.guessNumber < initVal.oracleN) sender() ! Response(+1)
                         unstashAll()
                         if (initVal.currentN == initVal.playerN) {
                              initVal.currentN = 0
                              initVal.turn += 1
                              context.parent ! GuiUpdateInt(GUIActor.CURRENT_TURN, initVal.turn)
                              initVal.ticketDispenser ! TicketDispenser.CHANGE_TURN
                         }
                    }
               }
          case _ => log.info("received unknown message")
     }

     def putInStop: Receive = {
          case msg: Start =>
               initVal = msg
               spawnPlayers()
               context.unbecome()
     }

     def spawnPlayers(): Unit = {
          playerList = List.fill(initVal.playerN)(context.actorOf(Props(new Player(initVal.ticketDispenser))))
          playerList.foreach(p => p ! Player.START)
     }

     def unstashAndKillAllPlayersAndTicketDispenser(): Unit = {
          unstashAll()
          playerList.foreach(p => p ! NumberGuessed(sender().path))
          context.stop(initVal.ticketDispenser)
     }

     def signalNewTurn(): Unit = {
          initVal.ticketDispenser ! TicketDispenser.CHANGE_TURN
     }
}

class Start(var playerN: Int, var ticketDispenser: ActorRef) {
     var oracleN: Int = new Random().nextInt(GUIActor.MAX_RAND)
     var turn: Int = 0
     var currentN = 0
     var numberWasGuessed = false
     var ticketDispense: ActorRef = ticketDispenser
}

case class Guess(var ticketN: Int, var guessNumber: Int)

case class Ticket(var ticketN: Int)

case class Response(var response: Int)

case class NumberGuessed(var winnerPath: ActorPath)

case class GuiUpdateInt(var message: String, var value: Int)

case class GuiUpdateString(var message: String, var value: String)

class GUIActor extends Actor {

     var oracle: ActorRef = context.actorOf(Props[Oracle])
     var gui: GameGUI = new GameGUI(context.self)
     gui.setVisible(true)
     defineVisibilities(true, false, false)

     override def receive: Receive = {
          case GUIActor.START =>
               defineVisibilities(false, true, false)
               oracle ! new Start(GUIActor.PLAYER_N, context.actorOf(Props[TicketDispenser]))
          case GUIActor.STOP =>
               defineVisibilities(false, false, true)
               oracle ! Oracle.STOP
          case GUIActor.RESET =>
               defineVisibilities(true, false, false)
               oracle ! Oracle.STOP
               oracle = context.actorOf(Props[Oracle])
               gui.setTurnArea("")
               gui.setNumberArea("")
               gui.setWinnerArea("")
          case msg: GuiUpdateInt => {
               if (msg.message == GUIActor.CURRENT_TURN) gui.setTurnArea(msg.value.toString)
               else if (msg.message == GUIActor.N_TO_GUESS) gui.setNumberArea(msg.value.toString)
          }
          case msg: GuiUpdateString => {
               defineVisibilities(false, false, true)
               gui.setWinnerArea("Player " + msg.value)

          }
     }

     def defineVisibilities(start:Boolean, stop:Boolean, reset:Boolean): Unit = {
          gui.startIsValid(start)
          gui.stopIsValid(stop)
          gui.resetIsValid(reset)
     }
}

object Oracle {
     val STOP = "stop"
}

object GUIActor {
     val START: String = "start"
     val STOP: String = "stop"
     val RESET: String = "reset"
     val CURRENT_TURN: String = "turn"
     val N_TO_GUESS: String = "ntoguess"
     val WINNER: String = "winner"
     val PLAYER_N = 3
     val MAX_RAND: Int = 10000000
}

object Main extends App {
     val system = ActorSystem("SYS")
     system.actorOf(Props[GUIActor])
}
