import akka.actor.{Actor, Stash}
import akka.event.Logging

class TicketDispenser() extends Actor with Stash {

     val log = Logging(context.system, this)

     var currentTicket: Int = _
     initTicketCounter()

     override def receive: Receive = {
          case TicketDispenser.CHANGE_TURN =>
               initTicketCounter()
               unstashAll()
          case TicketDispenser.GET_TICKET =>
               incrementTicket()
          case TicketDispenser.DIE =>
               initTicketCounter()
          case _ => log.info("received unknown message")
     }

     def initTicketCounter() : Unit = {
          currentTicket = -1
     }

     def incrementTicket(): Unit = {
          currentTicket += 1
          if (currentTicket >= GUIActor.PLAYER_N) {
               stash()
          } else {
               sender() ! Ticket(currentTicket)
          }
     }
}

object TicketDispenser {
     val CHANGE_TURN: String = "newturn"
     val GET_TICKET: String = "getticket"
     val DIE: String = "die"
}
