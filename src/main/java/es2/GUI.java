package es2;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;

public class GUI extends JFrame implements ActionListener {

    private JButton startButton;
    private JButton stopButton;
    private JButton resetButton;
    private JLabel  turnArea;
    private JLabel  winnerArea;
    private JLabel  numberArea;

    private AbstractVerticle guiActor;
    private Vertx vertx;
    private final EventBus eb;

    public GUI(AbstractVerticle guiActor, Vertx vertx) {
        this.guiActor = guiActor;
        this.vertx = vertx;
        eb = vertx.eventBus();
        setSize(1000, 100);
        setResizable(false);

        this.startButton = new JButton("Start");
        this.stopButton = new JButton("Stop");
        this.resetButton = new JButton("Reset");
        this.startButton.addActionListener(this);
        this.stopButton.addActionListener(this);
        this.resetButton.addActionListener(this);
        this.numberArea = new JLabel("");
        this.turnArea = new JLabel("");
        this.winnerArea = new JLabel("");

        JPanel panel = new JPanel();
        panel.add(startButton);
        panel.add(stopButton);
        panel.add(resetButton);

        JPanel panel1 = new JPanel();
        panel1.add(this.numberArea);
        panel1.add(this.turnArea);
        panel1.add(this.winnerArea);

        setLayout(new BorderLayout());
        add(panel, BorderLayout.NORTH);
        add(panel1, BorderLayout.SOUTH);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
                System.exit(-1);
            }
        });
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Start":
                eb.send(GuiVerticle.START, "");
                break;
            case "Stop":
                eb.send(GuiVerticle.STOP, "");
                break;
            case "Reset":
                eb.send(GuiVerticle.RESET, "");
                break;
            default:
                break;
        }
    }

    public void setNumberArea(String number) {
        SwingUtilities.invokeLater(() -> this.numberArea.setText("\t• N TO GUESS: " + number));
    }

    public void setTurnArea(String turn) {
        SwingUtilities.invokeLater(() -> this.turnArea.setText("\t• CURR TURN: " + turn));
    }

    public void setWinnerArea(String id) {
        SwingUtilities.invokeLater(() -> this.winnerArea.setText("\t• WINNER: " + id));
    }

    public void startIsValid(boolean flag) {
        SwingUtilities.invokeLater(() -> this.startButton.setEnabled(flag));
    }

    public void stopIsValid(boolean flag) {
        SwingUtilities.invokeLater(() -> this.stopButton.setEnabled(flag));
    }

    public void resetIsValid(boolean flag) {
        SwingUtilities.invokeLater(() -> this.resetButton.setEnabled(flag));
    }

}