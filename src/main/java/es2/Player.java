package es2;

import java.util.Random;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;

public class Player extends AbstractVerticle {

    public static final String TICKETS_AVAIL = "player.TicketsAvailable";
    public static final String NEW_TICKET = ".NewTicket";
    public static final String RESPONSE = ".Response";
    public static final String END_GAME = "player.EndGame";
    public static final String STOP = "everybody.stop";
    public static final int MAX_RAND = 10000;

    int guessN = new Random().nextInt(MAX_RAND), ticketN;
    String name;
    boolean haveIWon = false, gameEnded = false;

    public Player(String name) {
        this.name = "player." + name;
    }

    public void start() {

        EventBus eb = vertx.eventBus();

        // chiede ticket
        // da TICKDISP broadcast per nuovo turno -> chiede ticket a TICKDISP
        // da TICKDISP: riceve ticket -> fa guess a ORACLE e riceve reply per cosa fare la prossima volta
        // non riceve ticket -> non fa niente
        // da ORACLE riceve messaggio di riprovare a mandare?
        // da ORACLE endgame (stampa win o sob)

        eb.consumer(TICKETS_AVAIL, message -> {
            log("gonna go take tickets");
            onTicketsAvailable(eb);
        });

        eb.consumer(name + NEW_TICKET, message -> {
            ticketN = (int) message.body();
            guess(eb);
        });

        eb.consumer(name + RESPONSE, message -> {
            int response = (int) message.body();
            guessN += response;
        });

        eb.consumer(Player.END_GAME, message -> {
            gameEnded = true;
            if (name == message.body()) {
                haveIWon = true;
            }
            this.vertx.undeploy(this.deploymentID());
        });

        eb.consumer(Player.STOP, message -> {
            this.vertx.undeploy(this.deploymentID());
        });

        log(" has started with guess " + guessN);
    }

    public void stop() throws Exception {
        if (gameEnded) {
            if (haveIWon) {
                log("I WON!!!" + guessN);
            } else {
                log("BUHHHUU " + guessN);
            }
        }
    }

    private void onTicketsAvailable(EventBus eb) {
        eb.send(TicketDispenser.TICKET_REQ, name);
    }

    private void guess(EventBus eb) {
        DeliveryOptions options = new DeliveryOptions();
        options.addHeader("sender", name);
        options.addHeader("ticketN", Integer.toString(ticketN));
        options.addHeader("guessN", Integer.toString(guessN));
        eb.send(Oracle.GUESS, "", options);
    }

    public void log(String msg) {
        System.out.println("[• • • " + name + "] " + msg);
    }
}
