package es2;

import java.util.LinkedList;
import java.util.List;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;

public class GuiVerticle extends AbstractVerticle {

    private static final int playersN = 3;
    public static final String START = "start";
    public static final String STOP = "stop";
    public static final String RESET = "reset";
    public static final String CURRENT_TURN = "turn";
    public static final String N_TO_GUESS = "ntoguess";
    public static final String WINNER = "winner";
    private final GUI gui;
    private final Vertx vertx;
    private List<Player> list;
    private Oracle oracle;
    private TicketDispenser ticketDispenser;

    public GuiVerticle(Vertx vertx) {
        this.vertx = vertx;
        gui = new GUI(this, vertx);
        gui.setVisible(true);
        init();
    }

    private void init() {
        list = new LinkedList<>();
        oracle = new Oracle(playersN);
        ticketDispenser = new TicketDispenser(playersN);
        vertx.deployVerticle(oracle);
        vertx.deployVerticle(ticketDispenser);

        for (int i = 0; i < playersN; i++) {
            list.add(new Player(Integer.toString(i)));
        }

        for (Player p : list) {
            vertx.deployVerticle(p);
        }
    }

    @Override
    public void start() throws Exception {
        EventBus eb = vertx.eventBus();

        eb.consumer(START, message -> {
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! cose");
            eb.publish(Player.TICKETS_AVAIL, "");
            setEnabled(false, true, false);
        });

        eb.consumer(STOP, message -> {
            System.out.println("§§§§§§§§§§§§§§§§§ cose");
            eb.publish(Player.STOP, "");
            setEnabled(false, false, true);
        });

        eb.consumer(RESET, message -> {
            System.out.println("££££££ cose");
            init();
            gui.setTurnArea("");
            gui.setNumberArea("");
            gui.setWinnerArea("");
            setEnabled(true, false, false);
        });

        eb.consumer(CURRENT_TURN, message -> {
            gui.setTurnArea(String.valueOf((int)message.body()));
        });

        eb.consumer(N_TO_GUESS, message -> {
            gui.setNumberArea(String.valueOf(message.body()));
        });

        eb.consumer(WINNER, message -> {
            gui.setWinnerArea(String.valueOf(message.body()));
            setEnabled(false, false, true);
        });
    }

    private void setEnabled(boolean start, boolean stop, boolean reset) {
        gui.startIsValid(start);
        gui.stopIsValid(stop);
        gui.resetIsValid(reset);
    }
}
