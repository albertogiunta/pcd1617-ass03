package es2;

import io.vertx.core.Vertx;

public class Launcher {

    public static void main(String[] args) {
        Vertx  vertx  = Vertx.vertx();
        GuiVerticle guiVerticle = new GuiVerticle(vertx);
        vertx.deployVerticle(guiVerticle);
    }
}
