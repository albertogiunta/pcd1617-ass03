package es2;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;

public class TicketDispenser extends AbstractVerticle {

    public static final String  NAME = "TicketDispenser";
    public static final String  TICKET_REQ = NAME + ".TicketRequest";
    public static final String  NEWTURN_REQ = NAME + ".NewTurnRequest";

    int playerN;
    int current = 0;

    public TicketDispenser(int playerN) {
        this.playerN = playerN;
    }

    public void start() {
        EventBus eb = vertx.eventBus();

        // da PLAYER riceve richiesta, se < n nel turno reply con TICKET
        // altirmenti niente?
        // qui mai fuori ordine
        // da ORACLE nuovo turno manda broadcast a tutti PLAYER

        eb.consumer(NEWTURN_REQ, message -> {
            initCurrent();
            eb.publish(Player.TICKETS_AVAIL, "");
        });

        eb.consumer(TICKET_REQ, message -> {
            if (current < playerN) {
                eb.send(message.body().toString() + Player.NEW_TICKET, current);
                current++;
            }
        });

        eb.consumer(Player.END_GAME, message -> {
            current = 0;
            this.vertx.undeploy(this.deploymentID());
        });

        eb.consumer(Player.STOP, message -> {
            current = 0;
            this.vertx.undeploy(this.deploymentID());
        });

        log(NAME + " has started");
    }

    private void initCurrent() {
        current = 0;
    }

    public void log(String msg) {
        System.out.println("[• • TICK] " + msg);
    }

}
