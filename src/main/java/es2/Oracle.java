package es2;

import java.util.Random;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;

import static es2.Player.MAX_RAND;

public class Oracle extends AbstractVerticle {

    public static final String NAME = "oracle";
    public static final String GUESS = NAME + ".guess";

    int toGuessN = new Random().nextInt(MAX_RAND), playersN, currentTicket = 0, currentTurn = 0;
    boolean numberWasGuessed = false;

    public Oracle(int playersN) {
        this.playersN = playersN;
    }

    public void start() {

        EventBus eb = vertx.eventBus();

        // da PLAYER riceve guess e fa reply se è in ordine e se > o <
        // se uguale manda broadcast a tutti player con endgame e winner
        // se n = player manda a TICKDISP di cambiare turno
        // se fuori ordine manda broadcast a player che se non hanno fatto guess rimandano

        eb.consumer(Player.STOP, message -> {
            this.vertx.undeploy(this.deploymentID());
        });

        eb.consumer(GUESS, message -> {
            int ticketN = Integer.parseInt(message.headers().get("ticketN"));
            int playerGuess = Integer.parseInt(message.headers().get("guessN"));
            String sender = message.headers().get("sender");
            if (ticketN == currentTicket) {
                if (!numberWasGuessed) {
                    currentTicket++;
                    if (playerGuess == toGuessN) {
                        numberWasGuessed = true;
                        eb.send(GuiVerticle.WINNER, sender);
                        eb.publish(Player.END_GAME, sender);
                        vertx.undeploy(this.deploymentID());
                    } else {
                        if (playerGuess < toGuessN) {
                            eb.send(sender + Player.RESPONSE, +1);
                        } else if (playerGuess > toGuessN) {
                            eb.send(sender + Player.RESPONSE, -1);
                        }
                        if (currentTicket == playersN) {
                            eb.send(TicketDispenser.NEWTURN_REQ, "");
                            currentTicket = 0;
                            currentTurn++;
                            eb.send(GuiVerticle.CURRENT_TURN, currentTurn);
                        }
                    }
                }
            } else {
                if (!numberWasGuessed) {
                    DeliveryOptions options = new DeliveryOptions();
                    options.addHeader("sender", sender);
                    options.addHeader("ticketN", Integer.toString(ticketN));
                    options.addHeader("guessN", Integer.toString(playerGuess));
                    eb.send(Oracle.GUESS, "", options);
                }
            }
        });

        log(NAME + " has started with guess = " + toGuessN);

        eb.send(GuiVerticle.N_TO_GUESS, toGuessN);

    }


    public void log(String msg) {
        System.out.println("[• ORA] " + msg);
    }
}
