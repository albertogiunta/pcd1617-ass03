package es3;

import java.util.ArrayList;
import java.util.List;

import es3.acme.TemperatureSensorA1;
import es3.acme.TemperatureSensorB1;
import es3.acme.TemperatureSensorB2;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.functions.Predicate;

public class RoomMonitor {

    MonitorGUI view;
    TemperatureSensor           a1 = new TemperatureSensorA1();
    ObservableTemperatureSensor b1 = new TemperatureSensorB1();
    ObservableTemperatureSensor b2 = new TemperatureSensorB2();

    public RoomMonitor(MonitorGUI view) {
        this.view = view;
        init();
    }

    private void init() {
        Flowable<Double> source = Flowable.create(emitter -> {
            new Thread(() -> {
                double previous = -Double.MAX_VALUE;
                double current;
                while (true) {
                    try {
                        current = a1.getCurrentValue();
                        if (previous != current) {
                            emitter.onNext(current);
                            previous = current;
                        }
                    } catch (Exception ex) {
                    }
                }
            }).start();
        }, BackpressureStrategy.BUFFER);

        Predicate<Double> spikeCanceling = d -> d > -1000 && d < 1000;

        Flowable<Double> b1Flowable = b1.createObservable().filter(spikeCanceling);
        Flowable<Double> b2Flowable = b2.createObservable().filter(spikeCanceling);

        List<Flowable<?>> list = new ArrayList<>();
        list.add(b1Flowable);
        list.add(b2Flowable);
        Flowable<Boolean> warningFloawable = Flowable.combineLatest(list, objects -> (Double)objects[0] > 20 && (Double)objects[1] > 20);

        b1Flowable.subscribe(n -> view.setRoomB1(String.valueOf(n)));
        b2Flowable.subscribe(n -> view.setRoomB2(String.valueOf(n)));
        source.filter(spikeCanceling).subscribe(n -> view.setRoomA1(String.valueOf(n)));
        warningFloawable.subscribe(b -> view.setWarning(b));

    }

}