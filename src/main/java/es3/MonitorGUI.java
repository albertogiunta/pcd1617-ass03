package es3;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MonitorGUI extends JFrame implements ActionListener {

    private JLabel  roomA1;
    private JLabel  roomB2;
    private JLabel  roomB1;
    private JLabel  warning;

    private RoomMonitor controller;

    public MonitorGUI() {


        setVisible(true);
        setSize(1000, 50);
        setResizable(false);

        this.roomA1 = new JLabel("");
        this.roomB1 = new JLabel("");
        this.roomB2 = new JLabel("");
        this.warning = new JLabel("");

        JPanel panel1 = new JPanel();
        panel1.add(new JLabel("\t• A1: "));
        panel1.add(this.roomA1);
        panel1.add(new JLabel("\t• B1: "));
        panel1.add(this.roomB1);
        panel1.add(new JLabel("\t• B2: "));
        panel1.add(this.roomB2);
        panel1.add(new JLabel("\t• WARNING: "));
        panel1.add(this.warning);

        setLayout(new BorderLayout());
        add(panel1, BorderLayout.NORTH);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
            System.exit(-1);
            }
        });

        controller = new RoomMonitor(this);

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Start":
                break;
            case "Stop":
                break;
            case "Reset":
                break;
            default:
                break;
        }
    }

    public void setRoomA1(String value) {
        SwingUtilities.invokeLater(() -> this.roomA1.setText(value));
    }

    public void setRoomB1(String value) {
        SwingUtilities.invokeLater(() -> this.roomB1.setText(value));
    }

    public void setRoomB2(String value) {
        SwingUtilities.invokeLater(() -> this.roomB2.setText(value));
    }

    public void setWarning(Boolean showWarning) {
        if (showWarning) {
            SwingUtilities.invokeLater(() -> this.warning.setText("B1 & B2 over 20°"));
        } else {
            SwingUtilities.invokeLater(() -> this.warning.setText("none"));
        }
    }
}
