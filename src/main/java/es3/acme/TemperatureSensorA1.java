package es3.acme;

import es3.TemperatureSensor;

public class TemperatureSensorA1 extends TempSensorImpl implements TemperatureSensor {
    public TemperatureSensorA1() {
        super(5.0D, 15.0D, 1.0D, 0.05D);
    }
}

