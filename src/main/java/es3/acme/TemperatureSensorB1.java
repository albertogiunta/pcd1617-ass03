package es3.acme;

import es3.ObservableTemperatureSensor;
import io.reactivex.Flowable;

public class TemperatureSensorB1 extends ObservableTempSensorImpl implements ObservableTemperatureSensor {
    public TemperatureSensorB1() {
        super(0.0D, 10.0D, 1.8D, 0.1D);
    }

    public Flowable<Double> createObservable() {
        return this.createObservable(100);
    }
}