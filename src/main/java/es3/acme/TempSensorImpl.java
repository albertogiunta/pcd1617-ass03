package es3.acme;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class TempSensorImpl {
    private volatile double currentValue;
    private double min;
    private double max;
    private double spikeFreq;
    private Random gen = new Random(System.nanoTime());
    private BaseTimeValue time;
    private double zero;
    private double range;
    private double spikeVar;
    private UpdateTask updateTask;
    private ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);

    protected TempSensorImpl(double min, double max, double speed, double spikeFreq) {
        this.time = BaseTimeValue.getInstance(speed);
        this.zero = (max + min) * 0.5D;
        this.range = (max - min) * 0.5D;
        this.spikeFreq = spikeFreq;
        this.spikeVar = this.range * 10000.0D;
        this.updateTask = new TempSensorImpl.UpdateTask();
        this.exec.scheduleAtFixedRate(this.updateTask, 0L, 100L, TimeUnit.MILLISECONDS);
        this.updateTask.run();
    }

    public double getCurrentValue() {
        TempSensorImpl.UpdateTask var1 = this.updateTask;
        synchronized(this.updateTask) {
            return this.currentValue;
        }
    }

    public void shutdown() {
        this.exec.shutdownNow();
    }

    static class BaseTimeValue {
        static BaseTimeValue instance;
        private double time = 0.0D;
        private ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);

        static BaseTimeValue getInstance(double speed) {
            Class var2 = BaseTimeValue.class;
            synchronized(BaseTimeValue.class) {
                if(instance == null) {
                    instance = new BaseTimeValue(speed);
                }

                return instance;
            }
        }

        private BaseTimeValue(double speed) {
            this.exec.scheduleAtFixedRate(() -> {
                ScheduledExecutorService var3 = this.exec;
                synchronized(this.exec) {
                    this.time += 0.01D * speed;
                }
            }, 0L, 100L, TimeUnit.MILLISECONDS);
        }

        public double getCurrentValue() {
            ScheduledExecutorService var1 = this.exec;
            synchronized(this.exec) {
                return this.time;
            }
        }
    }

    class UpdateTask implements Runnable {
        UpdateTask() {
        }

        public void run() {
            double delta = (-0.5D + gen.nextDouble()) * range * 0.2D;
            double newValue = zero + Math.sin(time.getCurrentValue()) * range * 0.8D + delta;
            boolean newSpike = gen.nextDouble() <= spikeFreq;
            if(newSpike) {
                newValue = currentValue + spikeVar;
            }

            synchronized(this) {
                currentValue = newValue;
            }
        }
    }
}

