package es3;

public interface TemperatureSensor {
	
	double getCurrentValue();	
	
}
